-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: mealr-dev.ct9hptqyrqra.ap-south-1.rds.amazonaws.com:3306
-- Generation Time: Apr 05, 2022 at 12:25 PM
-- Server version: 8.0.28
-- PHP Version: 8.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kemuri`
--

-- --------------------------------------------------------

--
-- Table structure for table `stock_prices`
--

CREATE TABLE `stock_prices` (
  `id` int NOT NULL,
  `date` date NOT NULL,
  `stock_name` varchar(255) NOT NULL,
  `price` double(10,2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `stock_prices`
--

INSERT INTO `stock_prices` (`id`, `date`, `stock_name`, `price`, `created_at`, `updated_at`) VALUES
(1, '2020-02-11', 'AAPL', 320.00, '2022-04-04 21:51:04', '2022-04-04 21:51:04'),
(2, '2020-02-11', 'GOOGL', 1510.00, '2022-04-04 21:51:04', '2022-04-04 21:51:04'),
(3, '2020-02-11', 'MSFT', 185.00, '2022-04-04 21:51:04', '2022-04-04 21:51:04'),
(4, '2020-02-12', 'GOOGL', 1518.00, '2022-04-04 21:51:04', '2022-04-04 21:51:04'),
(5, '2020-02-12', 'MSFT', 184.00, '2022-04-04 21:51:04', '2022-04-04 21:51:04'),
(6, '2020-02-13', 'AAPL', 324.00, '2022-04-04 21:51:04', '2022-04-04 21:51:04'),
(7, '2020-02-14', 'GOOGL', 1520.00, '2022-04-04 21:51:04', '2022-04-04 21:51:04'),
(8, '2020-02-15', 'AAPL', 319.00, '2022-04-04 21:51:04', '2022-04-04 21:51:04'),
(9, '2020-02-15', 'GOOGL', 1523.00, '2022-04-04 21:51:04', '2022-04-04 21:51:04'),
(10, '2020-02-15', 'MSFT', 189.00, '2022-04-04 21:51:04', '2022-04-04 21:51:04'),
(11, '2020-02-16', 'GOOGL', 1530.00, '2022-04-04 21:51:05', '2022-04-04 21:51:05'),
(12, '2020-02-18', 'AAPL', 319.00, '2022-04-04 21:51:05', '2022-04-04 21:51:05'),
(13, '2020-02-18', 'MSFT', 187.00, '2022-04-04 21:51:05', '2022-04-04 21:51:05'),
(14, '2020-02-19', 'AAPL', 323.00, '2022-04-04 21:51:05', '2022-04-04 21:51:05'),
(15, '2020-02-21', 'AAPL', 313.00, '2022-04-04 21:51:05', '2022-04-04 21:51:05'),
(16, '2020-02-21', 'GOOGL', 1483.00, '2022-04-04 21:51:05', '2022-04-04 21:51:05'),
(17, '2020-02-21', 'MSFT', 178.00, '2022-04-04 21:51:05', '2022-04-04 21:51:05'),
(18, '2020-02-22', 'GOOGL', 1485.00, '2022-04-04 21:51:05', '2022-04-04 21:51:05'),
(19, '2020-02-22', 'MSFT', 180.00, '2022-04-04 21:51:05', '2022-04-04 21:51:05'),
(20, '2020-02-23', 'AAPL', 320.00, '2022-04-04 21:51:05', '2022-04-04 21:51:05');

-- --------------------------------------------------------

--
-- Table structure for table `stock_wallet`
--

CREATE TABLE `stock_wallet` (
  `id` int NOT NULL,
  `stock_name` varchar(255) NOT NULL,
  `balance` int NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `stock_wallet`
--

INSERT INTO `stock_wallet` (`id`, `stock_name`, `balance`, `created_at`, `updated_at`) VALUES
(2, 'AAPL', 100, '2022-04-05 09:39:59', '2022-04-05 11:39:07'),
(3, 'GOOGL', 10, '2022-04-05 09:50:59', '2022-04-05 12:24:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `stock_prices`
--
ALTER TABLE `stock_prices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stock_wallet`
--
ALTER TABLE `stock_wallet`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `stock_prices`
--
ALTER TABLE `stock_prices`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `stock_wallet`
--
ALTER TABLE `stock_wallet`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
