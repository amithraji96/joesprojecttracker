<?php
require __DIR__ . '/vendor/autoload.php';

use DI\Container;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Medoo\Medoo;

$container = new Container();

$container->set('database', function () {
    return new Medoo([
        'type' => 'mysql',
        'host' => 'mealr-dev.ct9hptqyrqra.ap-south-1.rds.amazonaws.com',
        'database' => 'kemuri',
        'username' => 'mealr_dev',
        'password' => 'L33t9999'
    ]);
});

AppFactory::setContainer($container);
$kemuri = AppFactory::create();

//set base path here in case you are running this project in any sub directory. 
// Eg: if project path is "www.domain.com/subfolder" then set "/subfolder" as base path without trailing slack("/").
$kemuri->setBasePath("/");

$twig = Twig::create(__DIR__.'/public/views', ['cache' => false]);
$kemuri->add(TwigMiddleware::create($kemuri, $twig));



//Including neccesary files
$folders = [
    __DIR__ . "/app/Core/Exceptions",
    __DIR__ . "/app/Routes",
    __DIR__ . "/app/Controllers",
    __DIR__ . "/app/Middlewares",
];

foreach($folders as $folder){
    foreach (glob("{$folder}/*.php") as $filename) {
        require $filename;
    }
}

$kemuri->run();