# JOE's Price Tracker #

This README would normally document whatever steps are necessary to get your application up and running.

### Setting Up ###

* clone the repository to server root.
* Incase its been added to a subdirectory, please change the subdirectory name in index.php
* Database: No DB setup needed. The project is already connected to my personal online hosted db. In case you want to access the sql file, its present in the root directory.
* Support php 7.2^ and mysql
* Apache server recommended. Incase of other servers, enable url rewriting and point any custom path to index.php.
* Composer update: There is a vendor folder packed in this repo. It is recommended to delete that manually and run a "composer update" on your machine to avoid any conflicts.

### About the Codebase ###

* Language: PHP
* Framework used: A micro framework (Slim v4) has been used that helps us with routing and templating to make things a bit easy.
* Libraries used:
				1) Database: Medoo PHP has been used to help us generate queries, handle db connections and also sanitize the inputs.
				
				2) Templating: Twig template engine has been used to hold templates in place making it easy to work in the code functions.
				
				
### Queries? ###

* Incase of queries, please contact me on amithraji@gmail.com
