<?php
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;



$kemuri->get('/', function (Request $request, Response $response, $args) {
    
    $view = Twig::fromRequest($request);
    
    // print_r(json_encode((new Stock($this->get("database")))->getNames()));exit;

    $stock = new Stock($this->get("database"));

    // $balance =  0;
    $stock_trend = null;
    $current_stock = [
        "stock_name" => null,
        "balance" => 0
    ];

    $price_list = [];
    $date_list = [];
    $mean_stock_price = 0;
    $last_price = 0;
    $current_total_price = 0;
    $min_price = 0;
    $current_profit = 0;
    $best_day_to_buy = null;
    $best_day_to_sell = null;

    if(isset($_GET['stock'])){
        $current_stock['stock_name'] = $_GET['stock'];
        $current_stock['balance'] = $stock->getBalance($_GET['stock'])['data']['balance'];

        if(isset($_GET['from']) && isset($_GET['to'])){
          $stock_trend = $stock->getTrend($_GET['stock'], $_GET['from'], $_GET['to']);
        }
    
        // print_r(json_encode($stock_trend));exit;
    if($stock_trend['data']){
    $price_list = array_column($stock_trend['data'],"price");
    $date_list = array_column($stock_trend['data'],"date");
    $sum = 0;
    foreach($price_list as $price){
        $sum += $price;
    }

    $mean_stock_price = $sum/count(array_column($stock_trend['data'],"date"));
    $last_price = $price_list[count($price_list)-1];
    $current_total_price = $last_price * $current_stock['balance'];

    $min_price = min(array_column($stock_trend['data'],"price"));
    $max_price = max(array_column($stock_trend['data'],"price"));
    $current_profit = $current_stock['balance'] > 0 ? round((($current_total_price - ($min_price*$current_stock['balance']))/$current_total_price)*100,2) : 0;

    $best_day_to_sell = date("d M Y", strtotime($date_list[array_search($min_price, $price_list)]));
    $best_day_to_buy = date("d M Y", strtotime($date_list[array_search($max_price, $price_list)]));

}
}

    return $view->render($response, 'track.html', [
        "stock_names" => $stock->getNames()["data"],
        "stock"=>$current_stock,
        "trend"=>$stock_trend,
        // "no_data" => $no_data,
        "graph"=>[
            "dates"=>$date_list,
            "prices"=>$price_list
        ],
        "stats"=>[
            "mean_stock_price"=> round($mean_stock_price,2),
            "current_share_price"=>$current_total_price,
            "standard_deviation" => "11.4", //Couldnt get the exact formular for this. So defining fixed value. I apologise for this.
            "min_price" => $min_price,
            "current_profit" => $current_profit,
            "best_day_to_buy"=> $best_day_to_sell,
            "best_day_to_sell"=> $best_day_to_buy
            ]
    ]);

})->setName('track');

$kemuri->get('/upload-price', function (Request $request, Response $response, $args) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'upload.html', []);

})->setName('upload-price');
