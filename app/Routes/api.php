<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Routing\RouteCollectorProxy;

$kemuri->group('/api/v1', function (RouteCollectorProxy $group) use($kemuri) {
    $group->post('/upload', function (Request $request, Response $response, $args) {
        $upload = new Upload($this->get('database'));
        
        $response->getBody()
        ->write(json_encode($upload->processCsv($_FILES)));

        return $response->withHeader('Content-Type', 'application/json')
        ->withStatus(200);

    })->setName('api.upload');
   
    $group->post('/transact', function (Request $request, Response $response, $args) {
        
        $stock = new Stock($this->get('database'));

        $request_payload = json_decode(file_get_contents("php://input"), true);

        $output = $request_payload['action'] == "buy" ? $stock->buy($request_payload['stock_name'], $request_payload['quantity']) : $stock->sell($request_payload['stock_name'], $request_payload['quantity']);
        
        $response->getBody()
        ->write(json_encode($output));

        return $response->withHeader('Content-Type', 'application/json')
        ->withStatus(200);

    })->setName('api.buy-sell');

});