<?php

class Upload{

    private $db;

    public function __construct($database){
        $this->db = $database;
    }

    public function processCsv(array $file){
        

        //declaring neccesary variables
        $file_binary = $file['csv']['tmp_name'];
        $read_file = fopen($file_binary, "r");
        $insert = [];

        //Debug variables
        $output = [];
        $error = null;

        $loop_index = 0;

        //While loop to read csv
        while (($column = fgetcsv($read_file, 10000, ",")) !== FALSE) {
            if($loop_index == 0){
                $loop_index++;
                continue;
            }
            
            //for some debug purpose
            $output[] = $column;

            //validating csv cells
            if(!isset($column[0]) || !isset($column[1]) || !isset($column[2])|| !isset($column[3])){
                $error =  ["status"=>false, "message"=>"This csv contains invalid values. No data was entered.", "data"=>null];
                break;
            }

            $price_exists = $this->db->count("stock_prices", [
                "date" => date("Y-m-d", strtotime($column[1])),
                "stock_name" => $column[2]
            ]);

            if(!$price_exists)
                $insert[] = [
                    "date" => date("Y-m-d", strtotime($column[1])),
                    "stock_name" => $column[2],
                    "price" => round((float) $column[3])
                ];
                

        }

        fclose($read_file);

        if($this->db->error)
            $error =  ["status"=>false, "message"=>"This csv contains invalid values. No data was entered.", "data"=>$database->errorInfo];
        

            if($error)
                return $error;


                //insert all at a go
                $this->db->insert("stock_prices",$insert);

        return ["status"=>true, "message"=>"Upload was successfull", "data"=>$output];

    }

}