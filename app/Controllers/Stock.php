<?php

class Stock{

    private $db;

    private $TRANSACTION_TYPE = ["buy"=>1,"sell"=>2];

    public function __construct($database){
        $this->db = $database;
    }

    public function getNames(){
        
        $output = $this->db->select("stock_prices","stock_name",[
            'GROUP' => 'stock_name',
        ]);
        return ["status"=>true, "message"=>"Here are the names.", "data"=>$output];

    }
    
    public function getBalance(string $stock_name){
        
        $output = $this->db->select("stock_wallet","balance",[
            'stock_name'=>$stock_name,
        ]);
        
        return ["status"=>true, "message"=>"Here are the names.", "data"=>[
            "balance"=> $output ? $output[0] : 0
        ]];

    }
    
    public function getTrend(string $stock_name, string $from, string $to){
        
        $output = $this->db->select("stock_prices","*",[
            'stock_name'=>$stock_name,
            'date[>]' => date("Y-m-d", strtotime($from)),
            'date[<]' => date("Y-m-d", strtotime($to)),
        ]);

        if($this->db->error)
            return ["status"=>false, "message"=>"There was some database error.", "data"=>$this->db->errorInfo];

        return ["status"=>true, "message"=>"Here is the data", "data"=>$output];

    }
   
    public function buy(string $stock_name, int $quantity){
        
        $stock_exists = $this->db->count("stock_wallet",[
            'stock_name'=>$stock_name,
        ]);

        if($stock_exists)
            $this->db->update("stock_wallet",[
                "balance[+]" => $quantity
            ],[
                'stock_name'=>$stock_name,
            ]);
        else
            $this->db->insert("stock_wallet",[
                'stock_name'=>$stock_name,
                "balance" => $quantity
            ]);

        if($this->db->error)
            return ["status"=>false, "message"=>"There was some database error.", "data"=>$database->errorInfo];

        return ["status"=>true, "message"=>"Stock has been bought.", "data"=>null];

    }
    
    
    public function sell(string $stock_name, int $quantity){
        
        $stock_balance = $this->db->select("stock_wallet","balance",[
            'stock_name'=>$stock_name,
        ]);

        if(!$stock_balance)
            return ["status"=>false, "message"=>"You dont have enough balance in this stock to sell.", "data"=>null];
        else{

            if($stock_balance[0] < $quantity)
                return ["status"=>false, "message"=>"You can't sell more than you own.", "data"=>$stock_balance];

            if($stock_balance[0] == 1)
            $this->db->delete("stock_wallet",[
                'stock_name'=>$stock_name,
            ]);
            else
                $this->db->update("stock_wallet",[
                    "balance[-]" => $quantity
                    ],[
                    'stock_name'=>$stock_name,
                    ]);
            
        }
            
        if($this->db->error)
            return ["status"=>false, "message"=>"There was some database error.", "data"=>$database->errorInfo];

        return ["status"=>true, "message"=>"Stocks has been sold", "data"=>null];

    }

}